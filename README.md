# AAR

AAR is a file format and archiver, written in Python. It generally produces smaller files than TAR or ZIP, but sacrifices the extra metadata that those formats carry.

---

## How do I get it?

If you want to download the archiver, head to the [Releases](https://github.com/fail0/AAR/releases) section to grab a 'frozen' binary, or the Python source.  
As long as you have Python 3 installed, there are no dependencies, so it can just be downloaded and ran.

The specification can be read [here](https://raw.githubusercontent.com/fail0/AAR/master/doc/AAR)

## Reasons to use AAR

 - Usually produces (slightly) smaller files than ZIP* or TAR
 - Python RI allows guaranteed compatibility between many platforms
 - Often faster in both creation and extraction than ZIP or TAR

<sub>* when not using compression</sub>

### Reasons not to use AAR

 - File metadata, such as c/m/a times, ownership, etc. is not stored
   - This will be added later
 - The implementation needs a lot more work
 - Because it is in its early stages, anything is liable to change at any time
 - The specification is a mess

## Usage Examples

Create an archive containing a folder and some files `aar c archive.aar AmazingFolder evil_plan.txt good_pasta_recipes.pdf`  
Extract an archive with verbose output, overwriting existing files `aar xvo archive.aar`  

---

    Thanks for using AAR :)
