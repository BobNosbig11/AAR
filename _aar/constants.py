# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

FORMAT_VERSION = b'\x01'
UTIL_VERSION = '-1'
