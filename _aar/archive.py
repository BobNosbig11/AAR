# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

if __name__ == '__main__':
	import sys
	print ("This file should not be ran directly.")
	sys.exit (1)

from _aar.constants import *
from _aar.vlq import *
from sys import exit

class Archive (object):
	def __init__ (self, filename, mode):
		''' Open the archive file, and run setup() or check () '''
		self.file = open (filename, mode)
		# Either create a new archive, or check that an existing archive is valid
		if mode == 'wb':
			self.setup ()
		elif mode == 'rb':
			self.check ()
		else:
			print ("Error: Attempted to open archive in an invalid mode (Required: 'rb' or 'wb' ; Got: " + mode + ')')
			sys.exit (1)

	def close (self):
		''' Close the file '''
		self.file.close ()

	def setup (self):
		''' Write the heder and TOC chunk, and create some variables '''
		# Write header
		self.file.write (b'\x7Faar!\x00' + FORMAT_VERSION + b'\x00\x00\x00\x00\x00\x00\x00\x00' + b'\xFF')
		# Write TOC chunk entry
		self.file.write (b'_toc\x00')
		# Initialise variables
		self.id_counter = 1
		self.file_stack = [] # FIXME: When working with a large amount of files, this will often crash due to OS limitations
		self.toc = {}
		self.toc_writeable = True

	def check (self):
		''' Perform some basic validity checks on the archive and read the contents of '''
		# Check magic number
		magic = self.file.read (6)
		if magic != b'\x7Faar!\x00':
			print ("Error: Invalid magic string (expected: aar! ; got: " + magic + " )\n      File: ", self.file.name)
		version = self.file.read (1)
		# Read the TOC and Data
		self.file.seek (16)
		self.data = self.file.read ()
		# Initialise variables
		self.data_started = False

	def add_directory (self, name, parent):
		''' Add a directory entry to the TOC '''
		if self.toc_writeable:
			self.id_counter += 1
			if parent == '/':
				parent_id = 1 # Root directory always has ID 1
			else:
				parent_id = self.toc[parent]
			self.file.write (b'd\x00' + bytes (name, 'utf-8') + b'\x00' + int_to_vlq (self.id_counter) + b'\x00' + int_to_vlq (parent_id) + b'\x00' + b'\x1E')
			if parent != '/':
				target = parent + '/' + name
			else:
				target = '/' + name
			self.toc[target] = self.id_counter
		else:
			print ("The TOC of this file cannot be written to right now.")

	def add_file (self, name, parent, _file):
		''' Add a file entry to the TOC, and store the file object in self.file_stack '''
		if self.toc_writeable:
			if parent == '/':
				parent_id = 1 # Root directory always has ID 1
			else:
				parent_id = self.toc[parent]
			_file.seek (0, 2)
			length = _file.tell ()
			_file.seek (0, 0)
			self.file.write (b'f\x00' + bytes (name, 'utf-8') + b'\x00' + int_to_vlq (parent_id) + b'\x00' + int_to_vlq (length) + b'\x00' + b'\x1E')
			if parent != '/':
				target = parent + '/' + name
			else:
				target = '/' + name
			self.file_stack.append (_file)
		else:
			print ("The TOC of this file cannot be written to right now.")

	def write_data (self):
		''' Write the data chunk '''
		self.toc_writeable = False
		self.file.write (b'\xFF_dat\x00')
		for _file in self.file_stack:
			self.file.write (_file.read ())

	def write_end (self):
		''' Write the end chunk '''
		self.file.write (b'\xFF_end\x00\xFF')

	def read_until_nul (self):
		''' Read from self.file until a NUL (0x00) is reached '''
		data = b''
		stop = False
		while not stop:
			byte = self.file.read (1)
			if byte == b'\x00':
				stop = True
			else:
				data += byte
		return data

	def parse_toc (self):
		''' Read the TOC and return it as a dictionary '''
		toc = {'dirs': {}, 'files': {}}
		self.file.seek (16)
		# Check that the chunk ID is correct
		chunk_id = self.file.read (4)
		if chunk_id != b'_toc':
			print ("The TOC in this file could not be found at offset 16.")
			exit (20)
		self.file.seek (1, 1)
		end = False
		while not end:
			record_type = self.file.read (1)
			if record_type == b'\xFF':
				end = True
			else:
				# Ran for both file and directory entries
				self.file.seek (1, 1)
				name = self.read_until_nul ().decode ('utf-8')
				if record_type == b'd':
					# Ran for only directory entries
					ID = vlq_to_int (self.read_until_nul ())
					ID_parent = vlq_to_int (self.read_until_nul ())
					toc['dirs'][ID] = {'name': name, 'id': ID, 'id_parent': ID_parent}
				elif record_type == b'f':
					# Ran for only file entries
					id_parent = vlq_to_int (self.read_until_nul ())
					length = vlq_to_int (self.read_until_nul ())
					target = str (id_parent) + '/' + name
					toc['files'][target] = {'length': length}
				# The end of the entry should always have been reached by this point
				byte = self.file.read (1)
				if byte != b'\x1E':
					print ("Warning: Did not reach the end of a TOC entry. This is likely to be very bad.")
		return toc

	def read_data (self, bytes):
		''' Read the data chunk '''
		if not self.data_started:
			chunk_id = self.file.read (4)
			# Check that the chunk ID is correct
			if chunk_id != b'_dat':
				print ("Error: The data section could not be found.")
				exit (21)
			self.file.seek (1, 1)
			self.data_started = True
		return self.file.read (bytes)
