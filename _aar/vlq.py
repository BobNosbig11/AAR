# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

if __name__ == '__main__':
	import sys
	print ("This file should not be ran directly.")
	sys.exit (1)

import binascii
import re

def int_to_vlq (n):
	bits = '{0:b}'.format (n)[::-1]
	bits = '{0:0{1}b}'.format (n, ((7 + len (bits) - 1) // 7) * 7)[::-1]
	vlq = '1_'.join (bits[i:i+7] for i in range (0, len (bits), 7))[::-1]
	vlq = '0' + vlq
	vlq_list = re.split ('[, _]+', vlq)
	vlq_bytes = b''
	for byte in vlq_list:
		byte = bytes (chr (int (byte, 2)), 'raw_unicode_escape')
		vlq_bytes += byte
	return vlq_bytes

def vlq_to_int (data):
	data = [data[i:i+1] for i in range (len (data))]
	byte_list = []
	for byte in data:
		byte = binascii.hexlify (byte)
		i = str (bin (int (byte, 16)))
		i = i.lstrip ('0b')
		byte_list += i
	byte_string = ''
	counter = 0
	for byte in reversed (byte_list):
		byte_string = byte + byte_string
		counter += 1
		if counter == 8:
			byte_string = '_' + byte_string
			counter = 0
	return int (''.join (byte_string.split ('_1')), 2)
