# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

if __name__ == '__main__':
	import sys
	print ("This file should not be ran directly.")
	sys.exit (1)

from _aar.constants import *
from _aar.create import create
from _aar.extract import extract
import sys

def help ():
	print ("""\
AAR

Usage:
 aar.py (c | x) [options] <archive> [files]

Options:
 -h --help         Show this text
 -V --version      Display version information
 -v --verbose      Print verbose information  (useful for debugging)
 -a --autoname     Name the output archive automatically (from the first file specified)
 -o --overwrite    Overwrite existing files and directories
 -n --no-recurse   Don't automatically add the contents of directories\
""")
	sys.exit (0)

def version ():
	print ("""\
AAR-utils Version %s
Copyright (C) 2018 George Gibson
Licensed under the MIT License\
""" % (UTIL_VERSION))
	sys.exit (0)

def ask_continue (message, default = 1):
	print (message)
	message = "Continue [" + ('Y' if default == 1 else 'y') + '/' + ('N' if default == 0 else 'n') + '] '
	option = input (message)
	if option == 'y' or option == 'Y' or (option == '' and default == 1):
		return 0
	elif option == 'n' or option == 'N' or (option == '' and default == 0):
		sys.exit (1)
	else:
		print ("Invalid input. Terminating...")
		sys.exit (1)

def main (args):
	del (args[0])
	if len (args) <= 0:
		help ()

	## PARSE ARGUMENTS ##

	opts = {'verbose': False, 'overwrite': False, 'autoname': False, 'no-recurse': False}
	optmap = {'v': 'verbose', 'o': 'overwrite', 'a': 'autoname', 'n': 'no-recurse'}

	if len (args[0]) > 1: # If arguments are being given with form 'cvn'
		_args = args[0]
		args[0] = args[0][:1]
		_args = _args[1:]
		for arg in _args:
			if arg == '-':
				pass
			elif arg == 'h':
				help ()
			elif arg == 'V':
				version ()
			elif arg in optmap:
				opts[optmap[arg]] = not opts[optmap[arg]]
			else:
				ask_continue ("Warning: Invalid argument " + arg + " passed.")
	else: # If arguments are being given with form 'c --verbose -n'
		counter = 0
		deletion_list = []
		for arg in args:
			if arg[:2] == '--': # Long options
				if arg[2:] == 'help':
					help ()
				elif arg[2:] == 'version':
					version ()
				elif arg[2:] in opts:
					opts[arg[2:]] = not opts[arg[2:]]
					deletion_list.append (counter)
				else:
					ask_continue ("Warning: Invalid argument " + arg + " passed.")
			elif arg[0] == '-': # Short options
				if arg[1] == 'h':
					help ()
				elif arg[1] == 'V':
					version ()
				elif arg[1] in optmap:
					opts[optmap[arg[1]]] = not opts[optmap[arg[1]]]
					deletion_list.append (counter)
				else:
					ask_continue ("Warning: Invalid argument " + arg + " passed.")
			counter += 1
		# Delete all the arguments that have been parsed
		deleted = 0
		for i in deletion_list:
			del (args[i - deleted])
			deleted += 1

	# Perform the requested action upon the archive
	if args[0] == 'c':
		del (args[0])
		create (opts, args)
	elif args[0] == 'x':
		del (args[0])
		extract (opts, args)
