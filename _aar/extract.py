# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

if __name__ == '__main__':
	import sys
	print ("This file should not be ran directly.")
	sys.exit (1)

from _aar.constants import *
from _aar.archive import Archive
from sys import exit
import os

paths = {}

def extract (opts, args):
	# Create an instance of the Archive class
	try:
		archive = Archive (args[0], 'rb')
	except IndexError:
		from _aar.main import help
		help ()
	except FileNotFoundError:
		print ("Error: The file '" + args[0] + "' does not exist.")
		exit (10)

	# Get the contents of the TOC in dictionary form
	toc = archive.parse_toc ()

	## EXTRACT DIRECTORIES ##
	# The first layer of directories (directories in the root of the archive) must be extracted seperately, because pass_directories cannot handle folders in the root
	for key, data in toc['dirs'].items ():
		if data['id_parent'] == 1:
			os.mkdir (data['name'])
			if opts['verbose']:
				print ("Extracted directory: " + data['name'])
			path = os.path.abspath (data['name'])
			paths[data['id']] = path
	# Extract all the other directories
	pass_counter = 1
	stop = False
	another_pass = True
	while not stop:
		if another_pass == True:
			pass_counter += 1
			another_pass = pass_directories (toc, pass_counter, opts)
		if another_pass == False:
			stop = True

	## EXTRACT FILES ##
	for path, data in toc['files'].items ():
		data = archive.read_data (data['length'])
		path = path.split ('/')
		if path[0] != '1': # ID 1 is root directory
			path = paths[int (path[0])] + '/' + path[1]
		else:
			path = path[1]
		if not os.path.isfile (path) or opts['overwrite'] == True:
			open (path, 'wb').write (data)
			if opts['verbose']:
				print ("Extracted file: " + os.path.abspath (path))
		else:
			from _aar.main import ask_continue
			ask_continue ("Error: File ' " + path + " ' already exists (I will not overwrite it unless the -o option is given)", 0)

def pass_directories (toc, ID, opts):
	another_pass = False
	for key, data in toc['dirs'].items ():
		if data['id_parent'] == ID:
			os.mkdir (paths[ID] + '/' + data['name'])
			if opts['verbose']:
				print ("Extracted directory: " + paths[ID] + '/' + data['name'])
			path = os.path.abspath (paths[ID] + '/' + data['name'])
			paths[data['id']] = path
		elif data['id_parent'] > ID:
			another_pass = True
	return another_pass
