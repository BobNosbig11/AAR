# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

if __name__ == '__main__':
	import sys
	print ("This file should not be ran directly.")
	sys.exit (1)

from _aar.constants import *
from _aar.archive import Archive
from re import split
import os

def _join (a, b):
	path = os.path.join (a, b)
	if os.path.sep != '/':
		path = path.replace ('\\', '/')
		print (path)
	return path

def create (opts, args):
	try:
		if not opts['autoname']:
			archive = Archive (args[0], 'wb')
		else:
			archive = Archive (args[0] + '.aar', 'wb')
	except IndexError:
		from _aar.main import help
		help ()

	if not opts['autoname']:
		del (args[0])

	dirs = {}

	for arg in args:
		stop = True
		if os.path.isdir (arg):
			if arg[-1] == '/':
				arg = arg[:-1]
			archive.add_directory (arg, '/')
			dirs[arg] = None
			if not opts['no-recurse']:
				for dirname, dirnames, filenames in os.walk (arg):
					for subdirname in dirnames:
						archive.add_directory (subdirname, '/' + dirname)
						if opts['verbose']:
							print ("Added directory: " + _join (dirname, subdirname))
							dirs[_join (dirname, subdirname)] = None
					for filename in filenames:
						archive.add_file (filename, _join ('.', dirname)[1:], open (_join (dirname, filename), 'rb'))
						if opts['verbose']:
							print ("Added file: " + _join (dirname, filename))
		else:
			_file = arg
			arg = arg.split ('/')
			if len (arg) > 1:
				name = arg[-1]
				del (arg[-1])
				dir_stack = ''
				if arg[0] not in dirs:
					archive.add_directory (arg[0], '/')
					dir_stack += '/' + arg[0]
				del (arg[0])
				for d in arg:
					if d not in dirs:
						archive.add_directory (d, dir_stack)
						dir_stack += '/' + d
						dirs[d] = None
						print (dir_stack)
				archive.add_file (name, dir_stack, open (_file, 'rb'))
			else:
				archive.add_file (_file, '/', open (_file, 'rb'))

	archive.write_data ()
	archive.write_end ()
	archive.close ()
