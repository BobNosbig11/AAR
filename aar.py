#!/usr/bin/env python
# encoding: utf-8

# Copyright (c) 2018 George Gibson
#
# Licensed under the MIT License
# See the accompanying file LICENSE for details

from _aar.main import main
from sys import argv

main (argv)
