#!/bin/sh

if [ $# -lt 1 ]; then
	echo "AAR mkrelease.sh\n\nUSAGE:\n ./mkrelease.sh <identifier>"
	exit 0
else
	echo "Making release: $1\n"
	echo " /// Creating stripped version of source tree"
	if [ -d releasetmp ]; then
		rm -Rf releasetmp
	fi
	mkdir releasetmp
	echo " -- Copying python files..."
	cp -vr _aar releasetmp
	cp -v aar.py releasetmp
	echo " -- Removing '__pycache__'..."
	rm -Rfv releasetmp/_aar/__pycache__
	echo " -- Copying LICENSE"
	cp -v LICENSE releasetmp
	echo " -- Archiving release - TAR"
	cd releasetmp
	tar cvf AAR-$1-src.tar *
	mv AAR-$1-src.tar ..
	echo " -- Archiving release - AAR"
	aar cv AAR-$1-src.aar *
	mv AAR-$1-src.aar ..
	cd ..
	echo " -- Deleting releasetmp/ folder..."
	rm -Rfv releasetmp
	echo " /// Creating frozen binaries"
	pyinstaller aar.py
	echo " -- Archive release - TAR"
	cd dist
	mv aar AAR
	tar cvf AAR-$1-bin_linux.tar AAR
	echo " -- Archive release - AAR"
	aar cv AAR-$1-bin_linux.aar AAR
	cd ..
	cp dist/AAR-$1* .
	echo " -- Deleting PyInstaller things"
	rm -Rfv build dist __pycache__ aar.spec
	echo " -- Compressing archives"
	for file in *.?ar; do
		xz -zv $file
	done
	echo "\n Done :)"
fi
